class Course
  attr_reader :name, :students, :department, :credits, :time_block, :days

  def initialize(name, department, credits, days=[], time_block=0)
    @name = name
    @department = department
    @credits = credits
    @days = days
    @time_block = time_block
    @students = []
  end

  def conflicts_with?(another_course)
    return false if self.time_block == 0 || self.days == []
    (self.time_block == another_course.time_block) && !(self.days & another_course.days).empty?
  end


  def add_student(student)
    student.enroll(self)
  end



end
