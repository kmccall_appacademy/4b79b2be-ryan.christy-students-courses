class Student
  attr_reader :first_name, :last_name

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
    @course_load = Hash.new(0)
  end

  def name
    "#{first_name} #{last_name}"
  end


  def courses
    @courses
  end

  #take a Course object, add it to the list of courses, and update the Course's list of enrolled students`
  #ignore attempts to re-enroll a student
  def enroll(course_name)
    raise "That course is in conflict with an existing course." if has_conflict?(course_name)
    if !(@courses.include?(course_name))
      @courses << course_name
      course_name.students << self
    end
  end

  def course_load
    @courses.each{ |course| @course_load[course.department] += course.credits }
    @course_load
  end


  def has_conflict?(a_course)
    @courses.each do |course|
      return true if course.conflicts_with?(a_course)
    end
    return false
  end

end
